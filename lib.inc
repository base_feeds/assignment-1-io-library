%define EXIT 60
%define NEW_STRING 0xA
%define SPACE 0x20
%define TAB 0x9
%define MIN 0x30
%define MAX 0x39
%define STDOUT 1
%define SYS_CALL 1



section .text 
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, EXIT
	syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax	

  .loop:		
	mov cl, [rdi + rax]
	test cl, cl
	jz .end	
				
	inc rax
	jmp .loop

  .end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	push rax
	push rsi
	push rdx

  .execute:	
	call string_length
	mov rdx, rax
	mov rsi, rdi
	mov rax, 1
	mov rdi, 1
	syscall

  .end: 
	pop rdx
	pop rsi
	pop rax
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rsi, '\n'

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rsi, rsp
	mov rax, SYS_CALL
	mov rdx, 1
	mov rdi, STDOUT
	syscall
	pop rdi
  .end:    
	ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push r10
	xor r10, r10
	push r11
	mov r11, 10
	mov rax, rdi

  .divide_number:
	inc r10
	xor rdx, rdx
	div r11
	add rdx, 30h
	push rdx

	test rax, rax
	jz .out_number
	jmp .divide_number

  .out_number:
	pop rdi
	call print_char

	dec r10
	test r10, r10
	jnz .out_number

  .end:
	pop r11
	pop r10
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
  .print_sign:
	mov rax, rdi
	sar rax, 63
	and rax, 0x1
	test rax, rax
	je .print_num
  .prepare_negative_num:
	push rdi
	mov rdi, 2Dh
	call print_char
	pop rdi
	neg rdi
  .print_num:
	call print_uint
  .end:
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	push rbx
	xor rbx, rbx
	xor rax, rax
	
  .loop_equal:
	mov cl, byte [rdi+rbx]
	cmp cl, byte [rsi+rbx]
	jnz .eq_err
	
	cmp byte [rdi+rbx], 0
	jz .eq_ok

	inc rbx
	jmp .loop_equal

  .eq_ok:
	mov rax, 1
	jmp .end
  .eq_err:
	xor rax, rax
	jmp .end

  .end:
	pop rbx
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push rdi
	push rsi
	push rdx
	push 0

	xor rax, rax
	xor rdi, rdi
	mov rsi, rsp 
	mov rdx, 1
	syscall

	test rax, rax
	je .end
	mov rax, [rsp]			
  .end:	
	pop rdx
	pop rdx
	pop rsi
	pop rdi
	ret 

; Принимает: адрес начала буфера(rdi), размер буфера(rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
  .delete_spaces:
	call read_char
	
	cmp rax, TAB
	je .delete_spaces
	cmp rax, NEW_STRING
	je .delete_spaces
	cmp rax, SPACE
	je .delete_spaces
	
  .before_loop:
	xor rdx, rdx
  .read_char_loop:
	cmp rdx, rsi
	je .str_error
	
	cmp rax, 0x00
	je .str_ok
	cmp rax, TAB
	je .str_ok
	cmp rax, NEW_STRING
	je .str_ok
	cmp rax, SPACE
	je .str_ok

	mov byte [rdi + rdx], al
	inc rdx
	call read_char
	jmp .read_char_loop

  .str_ok:
	mov byte [rdi + rdx], 0
	mov rax, rdi
	jmp .end
  .str_error:
	xor rax, rax
	jmp .end
  .end:	
 	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	push rbx

	xor rdx, rdx
	xor rax, rax
	xor rbx, rbx
  .loop:				
	mov bl, byte [rdi + rdx]  
	cmp bl, MIN
	jl .end
	cmp bl, MAX
	jg .end
	
	sub bl, MIN

	push rdx
	mov rdx, 10
	mul rdx
	pop rdx
	add rax, rbx

	inc rdx
	jmp .loop			

  .end:
	pop rbx
	ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	push rdi	
	
  .first_symb:
	mov rax, rdi
	
	cmp byte [rax], '-'
	jz .neg_number

	cmp byte [rax], '+'
	jz .pos_number

	call parse_uint
	jmp .end	

  .pos_number:
	inc rdi
	call parse_uint
	test rdx, rdx
	jz .end

	inc rdx
	jmp .end

  .neg_number:
	inc rdi
	call parse_uint
	test rdx, rdx
	jz .end

	inc rdx
	neg rax
	jmp .end

  .end:
	pop rdi
	ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi -> на строку, rsi -> на буфер, rdx -> на длину буфера
string_copy:
	xor rax, rax

	push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	
	cmp rax, rdx 
	jg .err_size
	
	push rax
	push r12

  .loop_copy:
	test rax, rax
	jl .end
	dec rax
	mov r12, [rdi+rax]	
	mov [rsi+rax], r12
	test rax, rax
	jz .end
	jmp .loop_copy

  .end:
	pop r12
	pop rax
  .err_size:
	xor rax, rax
	ret
